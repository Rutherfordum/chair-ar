﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;

public class CheckDeviceSupport : MonoBehaviour
{
    public Text statusAR;
    [SerializeField] ARSession m_Session = null;

    IEnumerator Start()
    {
        if ((ARSession.state == ARSessionState.None) || (ARSession.state == ARSessionState.CheckingAvailability))
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
            statusAR.text = "unsupported device";
        }

        else
        {
            statusAR.text = "";
            m_Session.enabled = true;
        }
    }
}
