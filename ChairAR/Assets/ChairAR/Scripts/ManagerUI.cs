﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
public class ManagerUI : MonoBehaviour
{
    private void Start()
    {
        go = objects[0];
    }
    private void Update()
    {
        if (activatemoders)
        {
            InputDevice.MoveRotateObject(go, sensetive);
            InputDevice.ScaleObject(go, 1, 10, 0.5f);
        }
    }

    #region open site
    public void AboutUs(string URL)
    {
        _Aboutus(URL);
    }
    private void _Aboutus(string URL)
    {
        Application.OpenURL(URL);
    }
    #endregion

    #region load scene
    public void LoadScene(int scene)
    {
        _LoadScene(scene);
    }
    private void _LoadScene(int scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
    #endregion

    #region change obj
    [Header("Change Objects")]
    public GameObject[] objects;
    private int currentObject;

    public void ChangeModel()
    {
        currentObject++;
        if (currentObject > objects.Length - 1 || currentObject < 0)
            currentObject = 0;

        foreach (GameObject obj in objects)
        {
            obj.SetActive(false);
        }

        objects[currentObject].SetActive(true);
        go = objects[currentObject];
        active = 0;
        moders = 0;
        activatemoders = false;
    }
    #endregion

    #region open site about objects
    private string URL;

    public void OpenSiteAboutObj()
    {
        switch (currentObject)
        {
            case 0:
                URL = "http://husmann.su/zerkleinerung-und-recycling/";
                break;
            case 1:
                URL = "http://husmann.su/recycling-umwelt-technik/prod/konvejery/konvejer-podayushchij-tsepnoj/";
                break;
            case 2:
                URL = "http://husmann.su/zerkleinerung-und-recycling/";
                break;
            case 3:
                URL = "http://husmann.su/recycling-umwelt-technik/prod/musorosortirovochnye-kompleksy/";
                break;
            case 4:
                URL = "http://husmann.su/recycling-umwelt-technik/prod/konvejery/";
                break;
            case 5:
                URL = "http://husmann.su/recycling-umwelt-technik/prod/konvejery/konvejer-sortirovochnyj/";
                break;
            case 6:
                URL = "http://husmann.su/recycling-umwelt-technik/prod/konvejery/transporter-lentochnyj/";
                break;
            case 7:
                URL = "http://husmann.su/recycling-umwelt-technik/prod/musorosortirovochnye-kompleksy/";
                break;
            case 8:
                URL = "http://husmann.su/gallery/musorosortirovochnyj-zavod-s-pressami-husmann-mp-1300-ivanovo/";
                break;
        }
        Application.OpenURL(URL);
    }
    #endregion

    #region max size objects
    [Header("Max size objects")]
    [Range(2, 20)]
    public float MaxSize = 10;
    [Range(0.1f, 1)]
    public float MinSize = 1;
    int active;

    public void MaxScaleObj()
    {
        // mode rotation disabled
        moders = 0;
        activatemoders = false;

        active += 1;
        if (active == 1)
        {
            go.transform.localScale = new Vector3(MaxSize, MaxSize, MaxSize);
        }
        else if (active == 2)
        {
            go.transform.localScale = new Vector3(MinSize, MinSize, MinSize);
            active = 0;
        }
    }
    #endregion

    #region mode roattion and size
    [Header("Mode rotate and size")]
    private GameObject go;
    [Tooltip("sensetive touch")]
    [Range(0, 1)]
    private float sensetive = 0.2f;
    int moders;
    bool activatemoders = false;
    public void RotationScaleMode()
    {
        //mode max size disable
        active = 0;

        moders++;

        if (moders == 1)
        {
            activatemoders = true;
        }
        else
        {
            activatemoders = false;
            moders = 0;
        }

    }
    #endregion

    #region mode move object
    [Header("Mode move object")]
    public GameObject interaction;
    public GameObject indicator;
    public GameObject objToPlace;
    
    public void MoveObject()
    {        
        interaction.SetActive(true);
        indicator.SetActive(true);
        objToPlace.SetActive(false);

        // mode rotation disabled
        moders = 0;
        activatemoders = false;

        //mode max size disable
        active = 0;
    }
    #endregion
}
