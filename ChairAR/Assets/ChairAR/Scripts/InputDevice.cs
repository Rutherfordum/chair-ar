﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDevice : MonoBehaviour
{
    static float Xrotate;
    static float DependetValue;
    static bool move;


    #region rotation of an object around its axis
    public static void MoveRotateObject(GameObject Object, float sensitivity)
    {
        if (Input.touchCount == 1)
        {
            Touch one1 = Input.GetTouch(0);
            Vector2 PosOne1 = one1.position - one1.deltaPosition;
            float x = PosOne1.x - one1.position.x;
            Xrotate += x;
            Object.transform.localRotation = Quaternion.Euler(0, Xrotate * sensitivity, 0);
        }
    }
    #endregion

    #region object resizing
    public static void ScaleObject(GameObject Object, float MinSize, float MaxSize, float sensitivity)
    {
        if (Input.touchCount == 2)
        {
            Touch zero = Input.GetTouch(0);
            Touch one = Input.GetTouch(1);
            Vector2 PosOne = one.position - one.deltaPosition;
            Vector2 PosZero = zero.position - zero.deltaPosition;
            float MagnOneAndTwo = (PosZero - PosOne).magnitude; //растояние между двумя точками т.е. находим его вектор
            float MagnOneAndTwoPos = (zero.position - one.position).magnitude;// также находим вектор толлько в зависимостит от касания это надо для обнуления координат 
            float Difference = MagnOneAndTwo - MagnOneAndTwoPos;//обнуление, это делается для того, чтобы при нажатиии повторный раз в другие позиции наш обьект не принемал их как новые а старался из исходного состояния изменять рамер
            DependetValue -= Difference / 100;
            if (DependetValue < MinSize)
            {
                DependetValue = MinSize;
            }
            if (DependetValue > MaxSize)
            {
                DependetValue = MaxSize;
            }
            Vector3 Target = new Vector3(Mathf.Clamp(DependetValue, MinSize, MaxSize), Mathf.Clamp(DependetValue, MinSize, MaxSize), Mathf.Clamp(DependetValue, MinSize, MaxSize));
            Object.transform.localScale = Vector3.MoveTowards(Object.transform.localScale, Target, sensitivity);
        }

    }
    #endregion

    #region Size Camera
    public static void SizeCamera(float MinSize=3.5f, float MaxSize=30f, float sensitivity=.5f)
    {
        if (Input.touchCount == 2) //if (Input.touchCount == 2)
        {
            move = false;
            Touch zero = Input.GetTouch(0);
            Touch one = Input.GetTouch(1);

            Vector2 PosOne = one.position - one.deltaPosition;
            Vector2 PosZero = zero.position - zero.deltaPosition;

            float MagnOneAndTwo = (PosZero - PosOne).magnitude; //растояние между двумя точками т.е. находим его вектор
            float MagnOneAndTwoPos = (zero.position - one.position).magnitude;// также находим вектор толлько в зависимостит от касания это надо для обнуления координат 

            float Difference = MagnOneAndTwo - MagnOneAndTwoPos;//обнуление, это делается для того, чтобы при нажатиии повторный раз в другие позиции наш обьект не принемал их как новые а старался из исходного состояния изменять рамер
            DependetValue += Difference / 100;

            if (Camera.main.orthographic)
            {
                if (DependetValue < MinSize)
                {
                    DependetValue = MinSize;
                }
                if (DependetValue > MaxSize)
                {
                    DependetValue = MaxSize;
                }
                Camera.main.orthographicSize = DependetValue; //* sensitivity;
            }
            else
            {
                if (DependetValue < MinSize)
                {
                    DependetValue = MinSize;
                }
                if (DependetValue > MaxSize)
                {
                    DependetValue = MaxSize;
                }
                Camera.main.fieldOfView = DependetValue * sensitivity;
            }
        }
    }
    #endregion

    #region mouve Camera
    static Vector3 TouchStart;
    public static void MoveCamera(float MinXY = -14f, float MaxXY = 14f)
    {
        if (Input.GetMouseButtonDown(0))//
        {
            move = true;
            TouchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.touchCount==1 && move==true)
        {
            Vector3 direct = TouchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Camera.main.transform.position += direct;

            if (Camera.main.transform.position.x < MinXY)
            {
                Camera.main.transform.position = new Vector3(MinXY, Camera.main.transform.position.y, Camera.main.transform.position.z);
            }
            if (Camera.main.transform.position.x > MaxXY)
            {
                Camera.main.transform.position = new Vector3(MaxXY, Camera.main.transform.position.y, Camera.main.transform.position.z);
            }
            if (Camera.main.transform.position.z < MinXY)
            {
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, MinXY);
            }
            if (Camera.main.transform.position.z > MaxXY)
            {
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, MaxXY);
            }
        }
    }
    #endregion

}