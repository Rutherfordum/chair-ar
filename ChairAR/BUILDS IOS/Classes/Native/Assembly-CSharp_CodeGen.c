﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ARTapToPlaceObject::Start()
extern void ARTapToPlaceObject_Start_mCEA5A9ADC5ACD785F329254FD679C5513136EDD4 ();
// 0x00000002 System.Void ARTapToPlaceObject::Update()
extern void ARTapToPlaceObject_Update_mCEA015466DB37FE1DC8947F8B16E5773004E3F7C ();
// 0x00000003 System.Void ARTapToPlaceObject::PlaceObject()
extern void ARTapToPlaceObject_PlaceObject_m72557EDFCEA7F7FDBE5E40E5ADE990B91FCB2EA7 ();
// 0x00000004 System.Void ARTapToPlaceObject::UpdatePlacementIndicator()
extern void ARTapToPlaceObject_UpdatePlacementIndicator_m03B360FB4F2108EE4568BEDD76425EEB98FC2B9C ();
// 0x00000005 System.Void ARTapToPlaceObject::UpdatePlacementPose()
extern void ARTapToPlaceObject_UpdatePlacementPose_m97C991933A4B270119305A0974E6D69D45D6640D ();
// 0x00000006 System.Void ARTapToPlaceObject::.ctor()
extern void ARTapToPlaceObject__ctor_mF5B2CD917913D8081CB42BA882632868D038267B ();
// 0x00000007 System.Collections.IEnumerator CheckDeviceSupport::Start()
extern void CheckDeviceSupport_Start_m8AC0F2A83A0C4B5C1BB9F7CD130DEC471ABBC889 ();
// 0x00000008 System.Void CheckDeviceSupport::.ctor()
extern void CheckDeviceSupport__ctor_mF0F725AAC1450C5C632A782B6E843B9DA613FD61 ();
// 0x00000009 System.Void InputDevice::MoveRotateObject(UnityEngine.GameObject,System.Single)
extern void InputDevice_MoveRotateObject_mB2130ADCD3CB947796373578A1569782AD1ABF6B ();
// 0x0000000A System.Void InputDevice::ScaleObject(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void InputDevice_ScaleObject_m23C2E0F67C295D7725E476B79988A2BBE1CBBB66 ();
// 0x0000000B System.Void InputDevice::SizeCamera(System.Single,System.Single,System.Single)
extern void InputDevice_SizeCamera_mB11479C5E5CAA25C1B2960089BB84127CEE079D7 ();
// 0x0000000C System.Void InputDevice::MoveCamera(System.Single,System.Single)
extern void InputDevice_MoveCamera_m175A5C6FB8B289FACC84720609E2AFE21D377E4C ();
// 0x0000000D System.Void InputDevice::.ctor()
extern void InputDevice__ctor_m160AEC8F7E9032FAF9577A6C884244BE764CC6DD ();
// 0x0000000E System.Void ManagerUI::Start()
extern void ManagerUI_Start_m166527CD0BD150375BF3E3B441A903F971388D4A ();
// 0x0000000F System.Void ManagerUI::Update()
extern void ManagerUI_Update_m6B0E0E7163685183E930092D42FCD84269300218 ();
// 0x00000010 System.Void ManagerUI::AboutUs(System.String)
extern void ManagerUI_AboutUs_mB47EAAC7C66C8EA2C25D05164B889AF31F4B2D32 ();
// 0x00000011 System.Void ManagerUI::_Aboutus(System.String)
extern void ManagerUI__Aboutus_m7C16FD6706760564C42D28F12604641A7B75514A ();
// 0x00000012 System.Void ManagerUI::LoadScene(System.Int32)
extern void ManagerUI_LoadScene_mE866F8237D2F71059D7D9586533D5F3DA8D066A3 ();
// 0x00000013 System.Void ManagerUI::_LoadScene(System.Int32)
extern void ManagerUI__LoadScene_m4F3BF0F7460D52E91FB5DCF69C51E8BC268E3752 ();
// 0x00000014 System.Void ManagerUI::ChangeModel()
extern void ManagerUI_ChangeModel_m859F901E5B755BD31CBDD1B0348AADB2D8A75D6E ();
// 0x00000015 System.Void ManagerUI::OpenSiteAboutObj()
extern void ManagerUI_OpenSiteAboutObj_m81E9D8649F7D8A4F32307606D5F8816148261231 ();
// 0x00000016 System.Void ManagerUI::MaxScaleObj()
extern void ManagerUI_MaxScaleObj_mD2F544B224E6475F144CC75B9B54BCA2F54D0D64 ();
// 0x00000017 System.Void ManagerUI::RotationScaleMode()
extern void ManagerUI_RotationScaleMode_mB58534873642E42F34735A2D2C2F7DDC82BC7C9D ();
// 0x00000018 System.Void ManagerUI::MoveObject()
extern void ManagerUI_MoveObject_m29EE87AD90D99B08D6620920E5B460334C057373 ();
// 0x00000019 System.Void ManagerUI::.ctor()
extern void ManagerUI__ctor_m7010330069734F29F3BDE79E3F3D74B75943DE5F ();
// 0x0000001A System.Void CheckDeviceSupport_<Start>d__2::.ctor(System.Int32)
extern void U3CStartU3Ed__2__ctor_m3F6948A49806BC788A634A944161FE454578AAE9 ();
// 0x0000001B System.Void CheckDeviceSupport_<Start>d__2::System.IDisposable.Dispose()
extern void U3CStartU3Ed__2_System_IDisposable_Dispose_m1D891B267233A1A3AB606D58976643A42C565B8D ();
// 0x0000001C System.Boolean CheckDeviceSupport_<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_mD6654533667336ACFA9FDE96DF5EE838D5AFAA22 ();
// 0x0000001D System.Object CheckDeviceSupport_<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9C749318B610A56819706A3CD1DEF11E159CDED ();
// 0x0000001E System.Void CheckDeviceSupport_<Start>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m66B5E020FE593219FB8275EE59AE4602EA62EABA ();
// 0x0000001F System.Object CheckDeviceSupport_<Start>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mB2E656C2194B82DD3ABC4F5CDD9F6C5D2A0DCE23 ();
static Il2CppMethodPointer s_methodPointers[31] = 
{
	ARTapToPlaceObject_Start_mCEA5A9ADC5ACD785F329254FD679C5513136EDD4,
	ARTapToPlaceObject_Update_mCEA015466DB37FE1DC8947F8B16E5773004E3F7C,
	ARTapToPlaceObject_PlaceObject_m72557EDFCEA7F7FDBE5E40E5ADE990B91FCB2EA7,
	ARTapToPlaceObject_UpdatePlacementIndicator_m03B360FB4F2108EE4568BEDD76425EEB98FC2B9C,
	ARTapToPlaceObject_UpdatePlacementPose_m97C991933A4B270119305A0974E6D69D45D6640D,
	ARTapToPlaceObject__ctor_mF5B2CD917913D8081CB42BA882632868D038267B,
	CheckDeviceSupport_Start_m8AC0F2A83A0C4B5C1BB9F7CD130DEC471ABBC889,
	CheckDeviceSupport__ctor_mF0F725AAC1450C5C632A782B6E843B9DA613FD61,
	InputDevice_MoveRotateObject_mB2130ADCD3CB947796373578A1569782AD1ABF6B,
	InputDevice_ScaleObject_m23C2E0F67C295D7725E476B79988A2BBE1CBBB66,
	InputDevice_SizeCamera_mB11479C5E5CAA25C1B2960089BB84127CEE079D7,
	InputDevice_MoveCamera_m175A5C6FB8B289FACC84720609E2AFE21D377E4C,
	InputDevice__ctor_m160AEC8F7E9032FAF9577A6C884244BE764CC6DD,
	ManagerUI_Start_m166527CD0BD150375BF3E3B441A903F971388D4A,
	ManagerUI_Update_m6B0E0E7163685183E930092D42FCD84269300218,
	ManagerUI_AboutUs_mB47EAAC7C66C8EA2C25D05164B889AF31F4B2D32,
	ManagerUI__Aboutus_m7C16FD6706760564C42D28F12604641A7B75514A,
	ManagerUI_LoadScene_mE866F8237D2F71059D7D9586533D5F3DA8D066A3,
	ManagerUI__LoadScene_m4F3BF0F7460D52E91FB5DCF69C51E8BC268E3752,
	ManagerUI_ChangeModel_m859F901E5B755BD31CBDD1B0348AADB2D8A75D6E,
	ManagerUI_OpenSiteAboutObj_m81E9D8649F7D8A4F32307606D5F8816148261231,
	ManagerUI_MaxScaleObj_mD2F544B224E6475F144CC75B9B54BCA2F54D0D64,
	ManagerUI_RotationScaleMode_mB58534873642E42F34735A2D2C2F7DDC82BC7C9D,
	ManagerUI_MoveObject_m29EE87AD90D99B08D6620920E5B460334C057373,
	ManagerUI__ctor_m7010330069734F29F3BDE79E3F3D74B75943DE5F,
	U3CStartU3Ed__2__ctor_m3F6948A49806BC788A634A944161FE454578AAE9,
	U3CStartU3Ed__2_System_IDisposable_Dispose_m1D891B267233A1A3AB606D58976643A42C565B8D,
	U3CStartU3Ed__2_MoveNext_mD6654533667336ACFA9FDE96DF5EE838D5AFAA22,
	U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9C749318B610A56819706A3CD1DEF11E159CDED,
	U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m66B5E020FE593219FB8275EE59AE4602EA62EABA,
	U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mB2E656C2194B82DD3ABC4F5CDD9F6C5D2A0DCE23,
};
static const int32_t s_InvokerIndices[31] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	1199,
	1735,
	1736,
	1737,
	23,
	23,
	23,
	26,
	26,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	31,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
